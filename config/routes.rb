Rails.application.routes.draw do
  
  devise_for :users, controllers: { sessions: 'users/sessions' }

  post 'documents/create'
  get 'page/index'
  root to: 'page#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :companies do
    collection do 
      get 'search'
    end
  end

  resources :users do
    collection do 
      get 'search'
    end
  end

  resources :quotations do
	  member do 
	    get 'preview'
      get 'status'
      get 'contract'
      get 'annexe'
      get 'notification'
      get 'edit_status'
	  end
    
    collection do 
      get 'search'
    end
	end

  resources :exams do
    collection do 
      get 'search'
    end
  end
end
