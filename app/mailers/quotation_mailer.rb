class QuotationMailer < ApplicationMailer
  helper ApplicationHelper
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.quotation_mailer.new_quotation.subject
  #
  default :from => "ECUAMERICAN <info@ecua-american.com>"
  def new_quotation(quotation)
  	@quotation = quotation
    @approver_users = Role.find_by_name("approver").users.map{|u| u.email}
    @saler = @quotation.company.user

    mail to: @approver_users, subject: "Nueva Cotización para #{@quotation.company.name}"
  end

  def quotation_approved(quotation)
    @quotation = quotation
    @approver = @quotation.approved_user
    @saler = @quotation.company.user
    mail to: @saler.email, subject: "Cotización aprobada para #{@quotation.company.name}"
  end

  def quotation_rejected(quotation)
    @quotation = quotation
    @approver = @quotation.rejected_user
    @saler = @quotation.company.user
    mail to: @saler.email, subject: "Cotización rechazada para #{@quotation.company.name}"
  end

  def quotation_pdf(quotation, email_to_send = [], body_to_send = nil)
    @exam_types = [["Sin asignar","1"],["Rayos X","2"],["Laboratorio","3"],["Imagen","4"],["Especialidades","5"]]
    @quotation = quotation
    @approver = User.last
    @saler = @quotation.company.user
    @saler = @quotation.created_by
    email = Rails.env.production? ? email_to_send : ["marcelo.toapanta@hotmail.com"]
    email = email_to_send
    @body_to_send = body_to_send


    @exams_item = @quotation.quotation_items.map{|e|
      {
        description: e.exam.full_name,
        type: e.exam.exam_type_id,
        quantity: e.quantity,
        value: e.value,
        total: e.total  
      }
    }

    @exams_item = @exams_item.group_by { |d| d[:type] }

    @filename = "#{@quotation.company.name}.pdf"
    attachments["#{@filename}"] = WickedPdf.new.pdf_from_string(
      render_to_string('quotations/preview.pdf.erb'), lowquality: true, margin:  {top: 0, bottom: 0, left: 0, right: 0 }
    )

    mail to: email, subject: "Cotización para #{@quotation.company.name}"
  end

end
