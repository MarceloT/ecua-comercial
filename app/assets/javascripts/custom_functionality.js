function load_upload(){
  console.log("on load_upload==>");
  $('.document_upload').fileupload({
    dataType: 'json',
    options: {
      maxFileSize: 5000000,
      acceptFileTypes: /(\.|\/)(pdf|docx?|txt)$/i,
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      upload_container = $(this).parents(".upload-container");
      upload_container.find('.progress .progress-bar').css(
          'width',
          progress + '%'
      );
    },
    add: function (e, data) {
        upload_container = $(this).parents(".upload-container");
        console.log("on add");
        console.log("data " + data);
        upload_container.find('.pdf_url_new').hide();
        data.context = $('#file_name').val('Uploading...');
        data.submit();
    },
    done: function (e, data) {
        console.log("on done");
        console.log(data);
        upload_container = $(this).parents(".upload-container");
        if (data.result){
          pdf = data.result;
          upload_container.find('.pdf_url_new').attr("href", pdf.document.url);
          upload_container.find('.pdf_url_new').show();
          upload_container.find('.document_id').val(pdf.id);

          // $(this).parents("form").submit();
        }
        else{
          console.log("Si resultados");
        }
    }
  });
}


$(document).on('nested:fieldAdded', function(event){
  // this field was just inserted into your form
  var field = event.field; 
  // it's a jQuery object already! Now you can find date input
  // var dateField = field.find('.select_2');
  // and activate datepicker on it
  $('#row_items .select_2').select2({
    language: {
      noResults: function () {
        return "No se encontró resultados";
      }
    },
    placeholder: function(){
      $(this).data('placeholder');
    },
    containerCssClass: ':all:',
    theme: "bootstrap",
    dropdownParent: $('#modal .modal-body')
  }).on('change', function (evt) {
    console.log(evt);
  });
  console.log("on add new field");
  load_upload();
  console.log(field);
  addColumn(col_num);

})