module ApplicationHelper
	def link_to_show(url)
    link_to '<i class="material-icons">person</i>'.html_safe, url, rel: "tooltip", title: "Detalles", class: "btn btn-info"
  end

  def link_to_pdf(url)
    link_to '<i class="material-icons icon-24pt">picture_as_pdf</i>'.html_safe, url, :target => "_blank"
  end

  def link_to_edit(url)
    link_to '<i class="material-icons">edit</i>'.html_safe , url,  data: { modal: true }, rel: "tooltip", title: "Editar", class: "btn btn-success"
  end

  def link_to_delete(url)
    link_to '<i class="material-icons">close</i>'.html_safe , url, method: :delete, data: { confirm: 'Esta seguro de eliminar?' }, rel: "tooltip", title: "Eliminar", class: "btn btn-danger"
  end

  def select_array(array, select_item)
    element = array.find { |el| el[1] == select_item.to_s }
    element.present? ? element[0] : "Sin asignar"
  end

end
