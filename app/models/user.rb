class User < ApplicationRecord
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :validatable, authentication_keys: [:login]

  validates :login, presence: true, uniqueness: true
  validates :name, presence: true, uniqueness: true
  # validates :role_ids, presence: true
  has_many :companies
  attr_writer :login_user


  def login_user
    @login_user || self.login || self.email
  end

  def email_required?
	  false
	end

  def role_name
    if self.roles.first.present?
      if self.roles.first.name == "admin"
        "Administrador del Sistema"
      elsif self.roles.first.name == "approver"
        "Aprobador"
      elsif self.roles.first.name == "seller"
        "Comercial"
      end
    else
      "Sin Asignar"
    end
  end

	
end
