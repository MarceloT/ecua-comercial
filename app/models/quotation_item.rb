class QuotationItem < ActiveRecord::Base
	validates :exam_id, :presence => true
	belongs_to :quotation, :optional => true
	belongs_to :exam
end
