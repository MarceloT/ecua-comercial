class QuotationContract < ApplicationRecord
	belongs_to :quotation
	belongs_to :document
	validates_uniqueness_of :document_id
end
