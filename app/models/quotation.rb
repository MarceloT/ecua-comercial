class Quotation < ApplicationRecord
	
	validates :company_id, :presence => true

	validates :description, :presence => true
	# validates :total_users, :numericality => {:greater_than => 0}

	validates :addressed_to, :presence => true

	belongs_to :company
	accepts_nested_attributes_for :company


	has_many :quotation_items, foreign_key: "quotation_id", :dependent => :destroy
  has_many :exams, through: :quotation_items, source: :exam
  accepts_nested_attributes_for :quotation_items


  belongs_to :created_by, class_name: "User", foreign_key: "created_by_id"
  belongs_to :approved_user, class_name: "User", foreign_key: "approved_user_id", :optional => true
  belongs_to :rejected_user, class_name: "User", foreign_key: "rejected_user_id", :optional => true

  validates :exam_number, :numericality => { :greater_than => 0}


	has_many :quotation_contracts, foreign_key: "quotation_id", :dependent => :destroy
	accepts_nested_attributes_for :quotation_contracts, allow_destroy: true

	has_many :quotation_annexes, foreign_key: "quotation_id", :dependent => :destroy, class_name: "QuotationAnnexe"
	accepts_nested_attributes_for :quotation_annexes, allow_destroy: true

	has_many :quotation_columns, foreign_key: "quotation_id", :dependent => :destroy, class_name: "QuotationColumn"
	# accepts_nested_attributes_for :quotation_columns, allow_destroy: true, reject_if: proc { |att| att['title'].blank?
	accepts_nested_attributes_for :quotation_columns, allow_destroy: true

  # has_many :medical_profile_exams, foreign_key: "medical_profile_id", :dependent => :destroy
  # has_many :exams, through: :medical_profile_exams, source: :exam
  # accepts_nested_attributes_for :exams

	attr_accessor :step, :exam_number

	def saved_company?
		self.company_id.present? && self.company.valid?
	end

	def saved_quotation?
		quotation_items_valid = true
		quotation_items_valid = false if self.quotation_items.map{|q| q.valid?}.include?(false)
		self.description.present? && self.exam_number > 0 && quotation_items_valid
	end

	def saved_addressed?
		self.addressed_to.present?
	end
	
end
