class Exam < ApplicationRecord
	validates :name, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true

  has_many :exam_descriptions, foreign_key: "exam_id", :dependent => :destroy
  accepts_nested_attributes_for :exam_descriptions
  def full_name
    "#{self.code} #{self.name}"
  end
end
