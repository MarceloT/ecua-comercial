class Company < ApplicationRecord
	validates :name, :presence => true
	validates_uniqueness_of :name, :message => '%{value} ya fue registrado en el sistema'
	validates :ruc, :presence => true
	validates_uniqueness_of :ruc, :message => '%{value} ya fue registrado en el sistema'
	validates :user_id, :presence => true
	validate :ci_passport
	belongs_to :user

	has_many :quotations

	after_save :save_full_name 

	def save_full_name
		if self.user_id && self.user_id > 0
			new_full_name = "#{self.ruc} #{self.name} (#{self.user.name}) #{self.in_quotation.present? ? "*" : ""}"
		else
			new_full_name = "#{self.ruc} #{self.name} (Sin Asignar) #{self.in_quotation.present? ? "*" : ""}"
    end
    Company.skip_callback :save, :after, :save_full_name
    self.update_attribute(:full_name, new_full_name)
    Company.set_callback :save, :after, :save_full_name
  end

  def in_quotation
  	if self.quotations.any? && self.quotations.last.status <= 1
  		true
  	else
  		false
  	end
  end

  

  def ci_passport
		require "id_ecuador"
		if new_record?
			ci = self.ruc
			if ci.present?
				# Passport validation
				cedula = IdEcuador.new ci
				if !cedula.valid? || !ci.scan(/\D/).empty?
					errors_mjs = ci.scan(/\D/).empty? ? cedula.errors.join(", ") : "Solo se acepta valores numéricos"
					errors.add(:ruc, "(#{ci}) #{errors_mjs}")
				end
			end
		end
	end

	
end
