json.extract! company, :id, :name, :ruc, :active, :direction, :sector, :contact_name, :contact_email, :doctor_name, :doctor_email, :billing_name, :billing_email, :user_id, :description, :created_at, :updated_at
json.url company_url(company, format: :json)
