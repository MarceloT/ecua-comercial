json.extract! quotation, :id, :date, :description, :appoved_user_id, :date_approved, :company_id, :pdf, :validity, :total_users, :status, :addressed_to, :note, :created_at, :updated_at
json.url quotation_url(quotation, format: :json)
