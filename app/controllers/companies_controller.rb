class CompaniesController < ApplicationController
  before_action :set_company, only: [:show, :edit, :update, :destroy]
  before_action :get_users, only: [:new, :create, :edit, :update]
  before_action :get_sectors, only: [:new, :create, :edit, :update, :show, :index, :search]
  respond_to :html

  # GET /companies
  # GET /companies.json
  def index
    # @companies = Company.all
    @companies = Company.all
    if !current_user.has_role?(:admin) && !current_user.has_role?(:approver)
      @companies = @companies.where(:user_id => current_user.id)
    end

    @companies = @companies.order(:name).page(params[:page]).per(20)
  end

  def search
    if params[:search] && params[:search][:q].present?
      @companies = Company.where("name ILIKE ? OR ruc ILIKE ? ", "%#{params[:search][:q]}%", "%#{params[:search][:q]}%")
    else
      @companies = Company.all
    end
    @companies = @companies.order(:name).page(params[:page]).per(20)
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
  end

  # GET /companies/new
  def new
    @company = Company.new
    @company.user_id = current_user.id
    @company.active = true
    respond_modal_with @company
  end

  # GET /companies/1/edit
  def edit
    respond_modal_with @company
  end

  # POST /companies
  # POST /companies.json
  def create
    @company = Company.create(company_params)

    @company_registered_by_ci = Company.where(ruc: company_params[:ruc]).last
    @company_registered_by_name = Company.where(name: company_params[:name]).last
    respond_modal_with @company, location: companies_path
  end

  # PATCH/PUT /companies/1
  # PATCH/PUT /companies/1.json
  def update
    @company.update(company_params)
    respond_modal_with @company, location: companies_path
  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    # @company.destroy
    @company.active = false
    @company.save
    respond_to do |format|
      format.html { redirect_to companies_url, notice: 'Empresa desactivada correctamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.require(:company).permit(:name, :ruc, :active, :direction, :sector, :contact_name, :contact_email, :contact_phone, :doctor_name, :doctor_email, :doctor_phone, :billing_name, :billing_email, :billing_phone, :user_id, :description)
    end
end
