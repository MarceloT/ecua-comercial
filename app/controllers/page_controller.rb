class PageController < ApplicationController
	before_action :get_companies, :get_status, :get_users, only: [:index]
  def index

  	if current_user.has_role?(:admin) || current_user.has_role?(:approver)
      @company_count = Company.count
	  	@quotation_count = Quotation.count

    else
    	@company_count = Company.where(:user_id => current_user.id).count
	  	@quotation_count = Quotation.where(:created_by_id => current_user.id).count
    end

    role = Role.find_by_name("seller")
    @user_count = role.users.count
    @template_count = 1

    
  end
end
