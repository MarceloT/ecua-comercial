class ExamsController < ApplicationController
  before_action :set_exam, only: [:show, :edit, :update, :destroy]
  before_action :get_exam_types, only: [:edit, :update, :create, :new, :index]

  # GET /exams
  # GET /exams.json
  def index
    # @exams = Exam.all
    @exams = Exam.order(:name).page(params[:page]).per(20)
  end

  def search
    if params[:search] && params[:search][:q].present?
      @exams = Exam.where("name ILIKE ? OR code ILIKE ? ", "%#{params[:search][:q]}%", "%#{params[:search][:q]}%")
    else
      @exams = Exam.all
    end
    @exams = @exams.order(:name).page(params[:page]).per(20)
  end

  # GET /exams/1
  # GET /exams/1.json
  def show
  end

  # GET /exams/new
  def new
    @exam = Exam.new
    respond_modal_with @exam
  end

  # GET /exams/1/edit
  def edit
    respond_modal_with @exam
  end

  # POST /exams
  # POST /exams.json
  def create
    @exam = Exam.create(exam_params)
    flash[:notice] = 'Exámen creado correctamente.'
    respond_modal_with @exam, location: exams_path
  end

  # PATCH/PUT /exams/1
  # PATCH/PUT /exams/1.json
  def update
    @exam.update(exam_params)
    flash[:notice] = 'Exámen actualizado correctamente.'
    respond_modal_with @exam, location: exams_path
  end

  # DELETE /exams/1
  # DELETE /exams/1.json
  def destroy
    # @exam.destroy
    @exam.active = false
    @exam.save
    respond_to do |format|
      format.html { redirect_to exams_url, notice: 'Exámen desactivado correctamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_exam
      @exam = Exam.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def exam_params
      params.require(:exam).permit(:name, :code, :active, :description, :exam_type_id, 
        exam_descriptions_attributes: [:id, :exam_id, :name, :active, :_destroy])
    end
end
