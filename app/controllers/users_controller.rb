class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :get_roles, only: [:new, :create, :edit, :update]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
    if !current_user.has_role? :admin
      role = Role.find_by_name("seller")
      @users = role.users
    end
    @users = @users.order(:name).page(params[:page]).per(20)
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  def search
    @users = User.all
    if !current_user.has_role? :admin
      role = Role.find_by_name("seller")
      @users = role.users
    end

    if params[:search] && params[:search][:q].present?
      @users = @users.where("name ILIKE ? OR email ILIKE ? ", "%#{params[:search][:q]}%", "%#{params[:search][:q]}%")
    end

    @users = @users.order(:name).page(params[:page]).per(20)
  end

  # GET /users/new
  def new
    @user = User.new
    @user.active = true
    respond_modal_with @user
  end

  # GET /users/1/edit
  def edit
    respond_modal_with @user
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.create(user_params)
    Rails.logger.info "===>>>>>>>: #{@user.errors.full_messages}"
    respond_modal_with @user, location: users_path
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    @user.update(user_params)
    respond_modal_with @user, location: users_path
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    # @user.destroy
    @user.active = false
    @user.save
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User correctamente desactivado.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:id, :email, :name, :login, :active, :photo, :signature, :encrypted_password, :password, :role_ids)
    end
end
