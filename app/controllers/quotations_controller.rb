class QuotationsController < ApplicationController
  load_and_authorize_resource
  before_action :set_quotation, only: [:show, :edit, :update, :destroy, :preview, :status, :notification, :contract]
  before_action :get_companies, :get_users, :get_sectors, :get_exams, only: [:new, :create, :edit, :update, :search, :index]
  before_action :get_status, only: [:new, :create, :edit, :update, :show, :index, :search]
  before_action :get_exam_types, only: [:show, :preview]

  # GET /quotations
  # GET /quotations.json
  def index
    # @quotations = Quotation.all
    
    if !current_user.has_role? :admin
      @quotations = Quotation.where(:created_by_id => current_user.id)
    else
      @quotations = Quotation.all
    end

    @quotations = @quotations.order(created_at: :desc).page(params[:page]).per(20)

  end

  def search
    @quotations = Quotation.all
    if params[:search].present? && params[:search][:company_id].present?
      @quotations = @quotations.where(:company_id => params[:search][:company_id])
    end
    if params[:search].present? && params[:search][:user_id].present?
      @quotations = @quotations.where(:created_by_id => params[:search][:user_id])
    end
    if params[:search].present? && params[:search][:status].present?
      @quotations = @quotations.where(:status => params[:search][:status])
    end
    @quotations = @quotations.order(created_at: :desc).page(params[:page]).per(20)

  end

  # GET /quotations/1
  # GET /quotations/1.json
  def show

    @exams_item = @quotation.quotation_items.map{|e|
      {
        description: e.exam.name,
        type: e.exam.exam_type_id,
        quantity: e.quantity,
        value_0: e.value_0,
        value_1: e.value_1,
        value_2: e.value_2,
        value_3: e.value_3,
        value_4: e.value_4,
        total: e.total  
      }
    }

    @exams_item = @exams_item.group_by { |d| d[:type] }

    @columns = @quotation.quotation_columns.map{|q| q.title}

    if params[:modal].present?
      @modal = true
      respond_modal_with @quotation
    else
      respond_to do |format|
        format.html {}
      end
    end
  end

  def preview

    @exams_item = @quotation.quotation_items.map{|e|
      {
        description: e.exam.name,
        type: e.exam.exam_type_id,
        quantity: e.quantity,
        value_0: e.value_0,
        value_1: e.value_1,
        value_2: e.value_2,
        value_3: e.value_3,
        value_4: e.value_4,
        total: e.total  
      }
    }

    @exams_item = @exams_item.group_by { |d| d[:type] }

    @columns = @quotation.quotation_columns.map{|q| q.title}

    respond_to do |format|
      format.html  { render layout: false }
      format.pdf do
        render pdf: @quotation.id.to_s, disable_links: true, show_as_html: false, lowquality: true, margin:  {top: 0, bottom: 0, left: 0, right: 0 }
      end
    end
  end

  def notification
    email_to_send = params[:notification][:email].present? ? params[:notification][:email].split(",") : nil
    body_to_send = params[:notification][:body].present? ? params[:notification][:body] : nil
    Rails.logger.info "===>>> email_to_send: #{email_to_send}"
    QuotationMailer.quotation_pdf(@quotation, email_to_send, body_to_send).deliver_now if Rails.env.production? || Rails.env.development?

    respond_to do |format|
      format.html {redirect_to quotation_path(@quotation), notice: 'Notificación enviada correctamente.'}
    end
  end

  def status
    if params[:commit].present? && params[:commit] == "Aprobar"
      @quotation.status = 2
      @msj = "Cotización para #{@quotation.company.name} aprobada."
    elsif params[:commit].present? && params[:commit] == "Rechazar"
      @quotation.status = 4
      @msj = "Cotización para #{@quotation.company.name} rechazada."
    else params[:status].to_i.present?
      @quotation.status = params[:status].to_i
      if params[:status].to_i == 2
        @msj = "Cotización para #{@quotation.company.name} aprobada."
      elsif params[:status].to_i == 4
        @msj = "Cotización para #{@quotation.company.name} rechazada."
      end
    end

    @quotation.note = params[:quotation][:note] if params[:quotation].present?

    if @quotation.status == 2
      @quotation.approved_user_id = current_user.id
      @quotation.approved_date = Time.now
      QuotationMailer.quotation_approved(@quotation).deliver if Rails.env.production?
    elsif @quotation.status == 4
      @quotation.rejected_user_id = current_user.id
      @quotation.rejected_date = Time.now
      QuotationMailer.quotation_rejected(@quotation).deliver if Rails.env.production?
    end

    @quotation.save(:validate => false)

    flash[:notice] = @msj
    Rails.logger.info "===>>>>>>>: #{@quotation.errors.full_messages}"
    @quotations_redirect = "/quotations/search?search[status]=1"
    respond_to do |format|
      format.js  {}
      format.html {redirect_to @quotations_redirect}
    end
  end

  def contract
    @quotation.exam_number = 1
    @quotation.update(quotation_params)
    respond_to do |format|
      format.js  {}
      format.html {redirect_to root_path}
    end
  end

  def annexe
    @quotation.exam_number = 1
    @quotation.update(quotation_params)
    respond_to do |format|
      format.js  {}
      format.html {redirect_to root_path}
    end
  end

  def edit_status
    @quotation.step = 3
    respond_modal_with @quotation
  end

  # GET /quotations/new
  def new
    @other_companies = @all_companies.where.not(:user_id => current_user.id).map{|c| c.ruc}
    @other_companies = @other_companies + current_user.companies.map{|c| c.ruc if c.in_quotation}
    @quotation = Quotation.new

    @archive = params[:archive].present? ? true : false
    # @quotation.exam_ids = [1, 2] unless @quotation.exam_ids.any?
    flash[:notice] = nil
    
    if params[:ci].present?
      company = Company.where(:ruc => params[:ci].to_s.strip).last
    end

    if company.present? && company.user_id == current_user.id
      # Si la empresa le pertenese
      @quotation.company_id = company.id
    elsif !company.present?
      # Si es una nueva empresa
      @quotation.company = Company.new
      @quotation.company.user_id = current_user.id  
      @quotation.company.active = true
      if params[:ci].present?
        @quotation.company.ruc = params[:ci]
        @quotation.company.valid?
        if @quotation.company.ci_passport && @quotation.company.ci_passport.any?

        else
          @quotation.company.save(:validate => false)
          @quotation.company_id = @quotation.company.id
        end

      end
    elsif company.present? && company.user_id != current_user.id
      # Ci de empresa invalido
      @quotation.company = Company.new
      flash[:notice] = "Empresa con RUC #{company.ruc} asignada a #{company.user.name} (#{company.user.email})"
    end

    @quotation.step = @archive.present? ? 0 : 1
    @quotation.exam_number = 0
    @quotation.date = Time.now.to_date
    @quotation.validity = (Time.now + 5.days).to_date
    @quotation.total_users = 1
    @quotation.total = 0
    @quotation.status = 0

    @quotation.quotation_items << QuotationItem.new unless @quotation.quotation_items.any?
    @quotation.quotation_columns << QuotationColumn.new(:title => "Valor")
    @quotation.quotation_columns << QuotationColumn.new(:title => "")
    @quotation.quotation_columns << QuotationColumn.new(:title => "")
    @quotation.quotation_columns << QuotationColumn.new(:title => "")
    @quotation.quotation_columns << QuotationColumn.new(:title => "")

    respond_modal_with @quotation
  end

  # GET /quotations/1/edit
  def edit
    @quotation.step = 3
    respond_modal_with @quotation
  end

  # POST /quotations
  # POST /quotations.json
  def create

    @other_companies = @all_companies.where.not(:user_id => current_user.id).map{|c| c.ruc}
    @quotation = Quotation.new(quotation_params)

    if @quotation.step.to_i == 0 && params[:finish].present?
      @archive = true
      @quotation.status = 3
      @quotation.created_by_id = current_user.id
      @quotation.exam_number = 1
      @quotation.approved_user_id = current_user.id
      @quotation.approved_date = Time.now
      @quotation.addressed_to = @quotation.company.name
      if @quotation.valid?
        @quotation.save
        location = quotation_path(@quotation)
      else
        location = quotations_path
      end

    else
      @quotation.step = 1
      @quotation.exam_number = 0

      if @quotation.saved_company?
        @quotation.step = 2
        num_no_exam = 0
        if quotation_params[:quotation_items_attributes].present?
          quotation_params[:quotation_items_attributes].to_h.map{|q| num_no_exam  += 1 if q[1][:exam_id] == '' }
          @quotation.exam_number = quotation_params[:quotation_items_attributes].to_h.count - num_no_exam
        end

      end

      if @quotation.saved_quotation?
        @quotation.step = 3
      end
      if @quotation.saved_addressed? && params[:finish].present?
        @quotation.step = 4
      end

      if params[:preview_1].present?
        @quotation.step = 1
      end

      if params[:preview_2].present?
        @quotation.step = 2
      end
      
      if @quotation.step == 4 && params[:finish].present?
        @quotation.status = 1
        @quotation.created_by_id = current_user.id
        @quotation.save
        location = quotation_path(@quotation)
      else
        @quotation.valid?
        location = quotations_path
      end
    end

    # @quotation = Quotation.create(quotation_params)

    QuotationMailer.new_quotation(@quotation).deliver if @quotation.valid? && Rails.env.production?

    Rails.logger.info "===>>>>>>>: #{@quotation.errors.inspect}"
    respond_modal_with @quotation, location: location
  end

  # PATCH/PUT /quotations/1
  # PATCH/PUT /quotations/1.json
  def update
    @quotation.update(quotation_params)
    respond_modal_with @quotation, location: quotations_path
  end

  # DELETE /quotations/1
  # DELETE /quotations/1.json
  def destroy
    @quotation.destroy
    respond_to do |format|
      format.html { redirect_to quotations_url, notice: 'Quotation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quotation
      @quotation = Quotation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def quotation_params
      params.require(:quotation).permit(:email, :step, :date, :description, :appoved_user_id, :date_approved, :company_id, :pdf, :validity, :total_users, :status, :addressed_to, :note, :created_by_id, :total, 
        company_attributes:[:id, :name, :ruc, :active, :direction, :sector, :contact_name, :contact_email, :contact_phone, :doctor_name, :doctor_email, :doctor_phone, :billing_name, :billing_email, :billing_phone, :user_id, :description],
        quotation_items_attributes: [:id, :quotation_id, :exam_id, :value_0, :value_1, :value_2, :value_3, :value_4, :value_5, :quantity, :total, :result, :_destroy],
        quotation_contracts_attributes: [:id, :quotation_id, :description, :_destroy, :document_id],
        quotation_annexes_attributes: [:id, :quotation_id, :description, :_destroy, :document_id],
        quotation_columns_attributes: [:id, :title, :_destroy, :quotation_id])

    end
end
