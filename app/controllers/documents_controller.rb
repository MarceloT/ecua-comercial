class DocumentsController < ApplicationController
  before_action :authenticate_user!
  def index
  end

  def create
  	@document = Document.new
  	if params[:document]
  		Rails.logger.info "====>>>>>>> on params[:document]"
  		@document.document = params[:document]
  	end
  	respond_to do |format|
      if @document.save
        format.html { }
        format.json { render json: @document, status: :created }
      else
        format.html { }
        format.json { render status: :unprocessable_entity, success: false }
      end
    end
  end

  def delete
  end
end
