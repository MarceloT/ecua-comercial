require "application_responder"
class ApplicationController < ActionController::Base
	before_action :authenticate_user!
  self.responder = ApplicationResponder
	respond_to :html
  protect_from_forgery with: :exception

  
  before_action :set_date_format, :get_pending_quotation

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { head :forbidden, content_type: 'text/html' }
      format.html { redirect_to main_app.root_url, notice: "No tiene permisos para acceder a esta página." }
      format.pdf { redirect_to main_app.root_url, notice: "No tiene permisos para acceder a esta página." }
      format.js   { head :forbidden, content_type: 'text/html' }
    end
  end

  def respond_modal_with(*args, &blk)
    options = args.extract_options!
    options[:responder] = ModalResponder
    respond_with *args, options, &blk
  end

  def get_users
    if current_user.has_role? :admin
      @all_users = User.all.order(:name)
    else
      role = Role.find_by_name("seller")
      @all_users = role.users
    end
  end

  def get_exams
    @all_exams = Exam.all.order(:name)
  end

  def get_roles
    @all_roles = Role.where(:name => ["approver", "seller"]).order(:name)
  end

  def get_companies
  	@all_companies = Company.select(:id, :full_name, :ruc).all.order(:name)
    
  end

  def get_sectors
    @sectors = [["Educación","0"],["Financiero","1"],["Industrial","2"],["Público","3"]]
  end

  def get_status
    @status = [["Elaboración","0"],["Pendiente","1"],["Aprobado","2"],["Finalizado","3"],["Rechazado","4"]]
  end

  def get_exam_types
    @exam_types = [["Sin asignar","1"],["Imagen Diagnóstica ","2"],["Laboratorio Clínico ","3"],["Vacunas","4"],["Valoraciones y Especialidades","5"]]
  end

  def get_pending_quotation
    if user_signed_in?
      @quotations_pending = Quotation.where("status IN (?)", [1])
      @quotations_approved = Quotation.where("status IN (?)", [2])
      @quotations_rejected = Quotation.where("status IN (?)", [4])

      if current_user.has_role? :admin
        @quotations_pending = @quotations_pending
        @quotations_approved = @quotations_approved
        @quotations_rejected = @quotations_rejected
      elsif current_user.has_role? :approver
        @quotations_pending = @quotations_pending
        @quotations_approved = @quotations_approved
        @quotations_rejected = @quotations_rejected
      elsif current_user.has_role? :seller
        @quotations_pending = @quotations_pending.where(:created_by_id => current_user.id)
        @quotations_approved = @quotations_approved.where(:created_by_id => current_user.id)
        @quotations_rejected = @quotations_rejected.where(:created_by_id => current_user.id)
      end

      @quotations_pending_count = @quotations_pending.count
      @quotations_approved_count = @quotations_approved.count
      @quotations_rejected_count = @quotations_rejected.count
    end 
  end


  def set_date_format
    @date_format = "%d-%m-%Y"
    @date_format_words = "%A, %-d %b %Y"
    @date_time_format = "%d-%m-%Y %I:%M"
  end


end
