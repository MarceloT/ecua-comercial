# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20200115221950) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.string "ruc"
    t.boolean "active"
    t.text "direction"
    t.integer "sector"
    t.string "contact_name"
    t.string "contact_email"
    t.string "doctor_name"
    t.string "doctor_email"
    t.string "billing_name"
    t.string "billing_email"
    t.integer "user_id"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "contact_phone"
    t.string "doctor_phone"
    t.string "billing_phone"
    t.integer "nextlab_id"
    t.string "full_name"
    t.index ["id"], name: "index_companies_on_id", unique: true
    t.index ["ruc"], name: "index_companies_on_ruc", unique: true
  end

  create_table "documents", force: :cascade do |t|
    t.string "name"
    t.string "document"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "exam_descriptions", force: :cascade do |t|
    t.string "name"
    t.boolean "active"
    t.integer "exam_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "exams", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.boolean "active"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "exam_type_id", default: 1
    t.index ["code"], name: "index_exams_on_code"
    t.index ["id"], name: "index_exams_on_id"
    t.index ["name"], name: "index_exams_on_name"
  end

  create_table "quotation_annexes", force: :cascade do |t|
    t.integer "quotation_id"
    t.integer "document_id"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "quotation_columns", force: :cascade do |t|
    t.integer "quotation_id"
    t.integer "order"
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "total"
  end

  create_table "quotation_contracts", force: :cascade do |t|
    t.integer "quotation_id"
    t.integer "document_id"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "quotation_items", force: :cascade do |t|
    t.integer "quotation_id"
    t.integer "exam_id"
    t.decimal "city_unit"
    t.decimal "province_unit"
    t.decimal "m_units_unit"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "total"
    t.integer "quantity"
    t.decimal "value_0"
    t.decimal "value_1"
    t.decimal "value_2"
    t.decimal "value_3"
    t.decimal "value_4"
    t.decimal "value_5"
    t.decimal "value_6"
    t.decimal "value_7"
    t.decimal "value_8"
    t.decimal "value_9"
  end

  create_table "quotations", force: :cascade do |t|
    t.datetime "date"
    t.text "description"
    t.integer "company_id"
    t.string "pdf"
    t.datetime "validity"
    t.integer "total_users"
    t.integer "status"
    t.string "addressed_to"
    t.text "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "total"
    t.integer "created_by_id"
    t.string "email"
    t.string "approver_document"
    t.integer "approved_user_id"
    t.datetime "approved_date"
    t.integer "rejected_user_id"
    t.datetime "rejected_date"
    t.index ["company_id"], name: "index_quotations_on_company_id"
    t.index ["id"], name: "index_quotations_on_id", unique: true
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "login"
    t.boolean "active", default: false
    t.string "photo"
    t.string "signature"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

end
