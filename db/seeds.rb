# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Users

user = User.create(login: 'andrea.maldonado', email: "andrea.maldonado@email.com", password: "123456", name: "Andrea Maldonado", active: true)
user.add_role "seller"
user = User.create(login: 'angelica.soto', email: "angelica.soto@email.com", password: "123456", name: "Angelica Soto", active: true)
user.add_role "seller"
user = User.create(login: 'angelina.gutierrez', email: "angelina.gutierrez@ecua-american.com", password: "123456", name: "Angelina Gutierrez", active: true)
user.add_role "seller"
user = User.create(login: 'angelita.martinez', email: "angelita.martinez@email.com", password: "123456", name: "Angelita Martinez", active: true)
user.add_role "seller"
user = User.create(login: 'sin.comercial', email: "sin.comercial@email.com", password: "123456", name: "Sin Comercial", active: true)
user.add_role "seller"
user = User.create(login: 'helen.arraez', email: "helen.arraez@email.com", password: "123456", name: "Helen Arraez", active: true)
user.add_role "seller"
user = User.create(login: 'klever.brito', email: "klever.brito@email.com", password: "123456", name: "Klever Brito", active: true)
user.add_role "seller"
user = User.create(login: 'mabel', email: "mabel@ecua-american.com", password: "123456", name: "Mabel Mero", active: true)
user.add_role "seller"
user = User.create(login: 'comercializacion1.uio', email: "comercializacion1.uio@ecua-american.com", password: "123456", name: "Marco Frenandez", active: true)
user.add_role "seller"
user = User.create(login: 'supervision.general', email: "supervision.general@ecua-american.com", password: "123456", name: "Maria de Lourdes Avila", active: true)
user.add_role "seller"
user = User.create(login: 'nancy.andrade', email: "nancy.andrade@ecua-american.com", password: "123456", name: "Nancy Andrade", active: true)
user.add_role "seller"
user = User.create(login: 'comercializacion.uio', email: "comercializacion.uio@ecua-american.com", password: "123456", name: "Nicole Perez", active: true)
user.add_role "seller"
user.add_role "approver"
user = User.create(login: 'agendamiento.ocupacional', email: "agendamiento.ocupacional@ecua-american.com", password: "123456", name: "Vanessa Mosquera", active: true)
user.add_role "seller"
user = User.create(login: 'marketinguio.uio', email: "marketinguio.uio@ecua-american.com", password: "123456", name: "Vanessa Perdomo", active: true)
user.add_role "seller"
user_to_nil = User.create(login: 'comercializacion15', email: "comercializacion15@ecua-american.com", password: "123456", name: "Maria Jose Buitron", active: true)
user_to_nil.add_role "seller"


user = User.create(login: 'admin', email: "admin@admin.com", password: "123456", name: "Administrador del Sistema", active: true)
user.add_role "admin"
user = User.create(login: 'aprobador', email: "aprobador@aprobador.com", password: "123456", name: "Aprobador", active: true)
user.add_role "approver"

p "Load Users"

# Companies
require 'csv'
datafile = Rails.root + 'db/companies.csv'
i = 1
CSV.foreach(datafile, headers: true) do |row|
  company = Company.where(:name => row[0]).last
  unless company.present?
    e = Company.new
    e.ruc = i.to_s.rjust(10, '0')
    e.name = row[1]
    e.user_id = row[2].present? ? (row[2] == 0 || row[2] == 5) ? user_to_nil.id : row[2] : nil
    e.nextlab_id = row[3].present? ? row[3] : nil
    e.active = true
    e.save(:validate => false)
    p "error => #{e.errors.full_messages}"
    i = i + 1
  end
end

p "Load Companies"

# Examns
require 'csv'
datafile = Rails.root + 'db/examenes.csv'
CSV.foreach(datafile, headers: true) do |row|
  exam = Exam.where(:name => row[0]).last
  unless exam.present?
    e = Exam.new
    e.code = row[0]
    e.name = row[1]
    e.active = row[2]
    e.save
  end
end

p "Load Examns"