class CreateExamDescriptions < ActiveRecord::Migration[5.1]
  def change
    create_table :exam_descriptions do |t|
      t.string :name
      t.boolean :active
      t.integer :exam_id

      t.timestamps
    end
  end
end
