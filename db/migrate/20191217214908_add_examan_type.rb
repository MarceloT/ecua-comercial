class AddExamanType < ActiveRecord::Migration[5.1]
  def change
  	add_column :exams, :exam_type_id, :integer, default: 1
  end
end
