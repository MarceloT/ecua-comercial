class AddNameToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :name, :string
    add_column :users, :login, :string
    add_column :users, :active, :boolean, default: false
    add_column :users, :photo, :string
    add_column :users, :signature, :string 
  end
end
