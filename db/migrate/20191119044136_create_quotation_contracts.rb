class CreateQuotationContracts < ActiveRecord::Migration[5.1]
  def change
    create_table :quotation_contracts do |t|
      t.integer :quotation_id
      t.integer :document_id
      t.string :description

      t.timestamps
    end
  end
end
