class CreateQuotationAnnexes < ActiveRecord::Migration[5.1]
  def change
    create_table :quotation_annexes do |t|
      t.integer :quotation_id
      t.integer :document_id
      t.string :description
      t.timestamps
    end

    create_table :documents do |t|
      t.string :name
      t.string :document
      t.timestamps
    end
  end
end
