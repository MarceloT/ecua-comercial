class CreateQuotations < ActiveRecord::Migration[5.1]
  def change
    create_table :quotations do |t|
      t.datetime :date
      t.text :description
      t.integer :appoved_user_id
      t.datetime :date_approved
      t.integer :company_id
      t.string :pdf
      t.datetime :validity
      t.integer :total_users
      t.integer :status
      t.string :addressed_to
      t.text :note

      t.timestamps
    end
    add_index :quotations, :id, unique: true
    add_index :quotations, :company_id
  end
end
