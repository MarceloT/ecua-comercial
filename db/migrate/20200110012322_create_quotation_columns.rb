class CreateQuotationColumns < ActiveRecord::Migration[5.1]
  def change
    create_table :quotation_columns do |t|
      t.integer :quotation_id
      t.integer :order
      t.string :title

      t.timestamps
    end
  end
end
