class AddQuotationValue < ActiveRecord::Migration[5.1]
  def change
  	add_column :quotations, :total, :decimal
  	add_column :quotations, :created_by_id, :integer
  	add_column :quotations, :email, :string
  end
end
