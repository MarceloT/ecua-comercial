class AddTotalColumn < ActiveRecord::Migration[5.1]
  def change
  	add_column :quotation_columns, :total, :decimal
  end
end
