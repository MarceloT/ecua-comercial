class AddIndexToCompanies < ActiveRecord::Migration[5.1]
  def change
  	add_column :companies, :full_name, :string
  	add_index :companies, :id, unique: true
  	add_index :companies, :ruc, unique: true
  end
end
