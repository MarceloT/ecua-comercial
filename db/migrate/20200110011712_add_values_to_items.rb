class AddValuesToItems < ActiveRecord::Migration[5.1]
  def change
  	remove_column :quotation_items, :value
  	add_column :quotation_items, :value_0, :decimal
  	add_column :quotation_items, :value_1, :decimal
  	add_column :quotation_items, :value_2, :decimal
  	add_column :quotation_items, :value_3, :decimal
  	add_column :quotation_items, :value_4, :decimal
  	add_column :quotation_items, :value_5, :decimal
  	add_column :quotation_items, :value_6, :decimal
  	add_column :quotation_items, :value_7, :decimal
  	add_column :quotation_items, :value_8, :decimal
  	add_column :quotation_items, :value_9, :decimal
  end
end
