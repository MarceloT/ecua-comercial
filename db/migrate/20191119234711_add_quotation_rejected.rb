class AddQuotationRejected < ActiveRecord::Migration[5.1]
  def change
  	remove_column :quotations, :appoved_user_id
  	remove_column :quotations, :date_approved

  	add_column :quotations, :approved_user_id, :integer
  	add_column :quotations, :approved_date, :datetime

  	add_column :quotations, :rejected_user_id, :integer
  	add_column :quotations, :rejected_date, :datetime
  end
end
