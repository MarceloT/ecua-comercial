class AddUserCountToQuotationItem < ActiveRecord::Migration[5.1]
  def change
  	add_column :quotation_items, :value, :decimal
  	add_column :quotation_items, :total, :decimal
  	add_column :quotation_items, :quantity, :integer
  	add_column :quotations, :approver_document, :string
  end
end
