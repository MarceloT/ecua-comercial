class AddPhoneToCompanies < ActiveRecord::Migration[5.1]
  def change
  	add_column :companies, :contact_phone, :string
  	add_column :companies, :doctor_phone, :string
  	add_column :companies, :billing_phone, :string
  end
end
