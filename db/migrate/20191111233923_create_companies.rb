class CreateCompanies < ActiveRecord::Migration[5.1]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :ruc
      t.boolean :active
      t.text :direction
      t.integer :sector
      t.string :contact_name
      t.string :contact_email
      t.string :doctor_name
      t.string :doctor_email
      t.string :billing_name
      t.string :billing_email
      t.integer :user_id
      t.text :description

      t.timestamps
    end
  end
end
